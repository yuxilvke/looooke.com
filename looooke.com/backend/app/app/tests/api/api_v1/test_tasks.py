from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app.core.config import settings
from app.tests.utils.task import create_random_task


def test_create_task(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    data = {"name": "Foo", "description": "Fighters", "url":"http://www.baidu.com"}
    response = client.post(
        f"{settings.API_V1_STR}/task/", headers=superuser_token_headers, json=data,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["name"] == data["name"]
    assert content["url"] == data["url"]
    assert content["description"] == data["description"]
    assert "id" in content
    assert "owner_id" in content


def test_read_task(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    task = create_random_task(db)
    response = client.get(
        f"{settings.API_V1_STR}/task/{task.id}", headers=superuser_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["name"] == task.name
    assert content["description"] == task.description
    assert content["id"] == task.id
    assert content["owner_id"] == task.owner_id
