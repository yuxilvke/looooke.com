from typing import Optional

from sqlalchemy.orm import Session

from app import crud, models
from app.schemas.task import TaskCreate
from app.tests.utils.user import create_random_user
from app.tests.utils.utils import random_lower_string, random_url


def create_random_task(db: Session, *, owner_id: Optional[int] = None) -> models.Task:
    if owner_id is None:
        user = create_random_user(db)
        owner_id = user.id
    url = random_url()
    name = random_lower_string()
    description = random_lower_string()
    task_in = TaskCreate(name=name, description=description, url=url)
    return crud.task.create_with_owner(db=db, obj_in=task_in, owner_id=owner_id)
