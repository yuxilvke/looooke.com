import decodeJwt from 'jwt-decode';
import { apiUrl } from './env';

const authUrl = apiUrl + '/api/v1/login/access-token';
const authProvider = {
    login: ({ username, password }) =>  {
        const body = encodeURIComponent("username") + '=' + encodeURIComponent(username) + '&' +
                     encodeURIComponent("password") + '=' + encodeURIComponent(password);
        const request = new Request(authUrl, {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }),
            body,
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ access_token }) => { 
                localStorage.setItem('token', access_token);
                
                const decodedToken = decodeJwt(access_token);
                localStorage.setItem('user_id', decodedToken.sub);
            });
    },
    logout: () => {
        localStorage.removeItem('token');
        localStorage.removeItem('user_id');
        return Promise.resolve();
    },
    checkError: (error) => {
        const status = error.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('token');
            localStorage.removeItem('user_id');
            return Promise.reject();
        }
        return Promise.resolve();
    },
    checkAuth: () => localStorage.getItem('token')
        ? Promise.resolve()
        : Promise.reject(),
    getPermissions: () => {
        const user_id = localStorage.getItem('user_id');
        return Promise.resolve(user_id);
    },
};

export default authProvider;