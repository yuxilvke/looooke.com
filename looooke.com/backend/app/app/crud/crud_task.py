from typing import List, Tuple

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.task import Task
from app.schemas.task import TaskCreate, TaskUpdate


class CRUDTask(CRUDBase[Task, TaskCreate, TaskUpdate]):
    def create_with_owner(
        self, db: Session, *, obj_in: TaskCreate, owner_id: int
    ) -> Task:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_multi_by_owner(
        self, db: Session, *, owner_id: int, _start:int=0, _end:int=100, _sort:str="id", _order:str="desc"
    ) -> Tuple[List[Task], int]:
        return db.query(self.model).filter(Task.owner_id == owner_id).offset(_start).limit(_end).all(), db.query(self.model).count()
        


task = CRUDTask(Task)
