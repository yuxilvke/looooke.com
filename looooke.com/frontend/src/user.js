import React from 'react';
import { List, Datagrid, TextField, BooleanField, EmailField, Edit, Create, SimpleForm, TextInput, BooleanInput } from 'react-admin';


export const UserList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="id" />
            <EmailField source="email" />
            <TextField source="full_name" />
            <BooleanField source="is_superuser" />
            <BooleanField source="is_active" />
        </Datagrid>
    </List>
);

export const UserEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="email" type="email" />
            <TextInput source="full_name" />
            <BooleanInput source="is_superuser" />
            <BooleanInput source="is_active" />
        </SimpleForm>
    </Edit>
);

export const UserCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="email" type="email" />
            <TextInput source="full_name" />
            <TextInput source="password" />
            <BooleanInput source="is_superuser" />
            <BooleanInput source="is_active" />
        </SimpleForm>
    </Create>
);