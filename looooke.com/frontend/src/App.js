import React from 'react';
import { Admin, Resource } from 'react-admin';
import { UserList, UserEdit, UserCreate } from './user';
import { TaskList, TaskCreate, TaskEdit } from './task';
import Dashboard from './Dashboard';
import authProvider from './authProvider';
import MydataProvider from './MydataProvider';


const App = () => (
    <Admin dataProvider={ MydataProvider } dashboard={Dashboard} authProvider={authProvider}>
        <Resource name="task" list={TaskList} edit={TaskEdit} create={TaskCreate} />
        <Resource name="user" list={UserList} edit={UserEdit} create={UserCreate} />
    </Admin>
);

export default App;