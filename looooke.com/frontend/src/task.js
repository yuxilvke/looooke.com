import React from 'react';
import { List, Datagrid, TextField, ReferenceField, Edit, Create, SimpleForm, TextInput } from 'react-admin';

export const TaskList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="url" />
            <TextField source="name" />
            <TextField source="id" />
            <TextField source="description" />
            <ReferenceField source="owner_id" reference="user">
                <TextField source="full_name" />
            </ReferenceField>
        </Datagrid>
    </List>
);

export const TaskEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="url" />
            <TextInput source="name" />
        </SimpleForm>
    </Edit>
);


export const TaskCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="url" />
            <TextInput source="name" />
        </SimpleForm>
    </Create>
);