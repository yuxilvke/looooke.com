from sqlalchemy.orm import Session

from app import crud
from app.schemas.task import TaskCreate, TaskUpdate
from app.tests.utils.user import create_random_user
from app.tests.utils.utils import random_lower_string, random_url


def test_create_task(db: Session) -> None:
    url = random_url()
    name = random_lower_string()
    description = random_lower_string()
    task_in = TaskCreate(name=name, description=description, url=url)
    user = create_random_user(db)
    task = crud.task.create_with_owner(db=db, obj_in=task_in, owner_id=user.id)
    assert task.name == name
    assert task.description == description
    assert task.owner_id == user.id


def test_get_task(db: Session) -> None:
    url = random_url()
    name = random_lower_string()
    description = random_lower_string()
    task_in = TaskCreate(name=name, description=description, url=url)
    user = create_random_user(db)
    task = crud.task.create_with_owner(db=db, obj_in=task_in, owner_id=user.id)
    stored_task = crud.task.get(db=db, id=task.id)
    assert stored_task
    assert task.id == stored_task.id
    assert task.name == stored_task.name
    assert task.description == stored_task.description
    assert task.owner_id == stored_task.owner_id


def test_update_task(db: Session) -> None:
    url = random_url()
    name = random_lower_string()
    description = random_lower_string()
    task_in = TaskCreate(name=name, description=description, url=url)
    user = create_random_user(db)
    task = crud.task.create_with_owner(db=db, obj_in=task_in, owner_id=user.id)
    description2 = random_lower_string()
    task_update = TaskUpdate(description=description2)
    task2 = crud.task.update(db=db, db_obj=task, obj_in=task_update)
    assert task.id == task2.id
    assert task.name == task2.name
    assert task2.description == description2
    assert task.owner_id == task2.owner_id


def test_delete_task(db: Session) -> None:
    url = random_url()
    name = random_lower_string()
    description = random_lower_string()
    task_in = TaskCreate(url=url, name=name, description=description)
    user = create_random_user(db)
    task = crud.task.create_with_owner(db=db, obj_in=task_in, owner_id=user.id)
    task2 = crud.task.remove(db=db, id=task.id)
    task3 = crud.task.get(db=db, id=task.id)
    assert task3 is None
    assert task2.id == task.id
    assert task2.name == name
    assert task2.description == description
    assert task2.owner_id == user.id
