import simpleRestProvider from 'ra-data-simple-rest';
import { fetchUtils } from 'react-admin';
import { stringify } from 'query-string';
import { apiUrl } from './env';

const fetchJson = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    // add your own headers here
    let token = localStorage.getItem('token');
    options.headers.set('Authorization', 'Bearer ' + token);
    return fetchUtils.fetchJson(url, options);
}

const dataProvider = simpleRestProvider(apiUrl + '/api/v1', fetchJson);

const myDataProvider = {
    ...dataProvider,
    getList: (resource, params) => {
        let url='';
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
            _start: (page - 1) * perPage,
            _end:  page * perPage - 1,
            _sort: field,
            _order: order
        };
        url = `${apiUrl}/api/v1/${resource}?${stringify(query)}`;

        const options = {
            headers : new Headers({
                Accept: 'application/json',
            }),
        };
        let token = localStorage.getItem('token');
        options.headers.set('Authorization', 'Bearer ' + token);
        let headers;
        return fetch(url, options)
        .then(res => { 
            headers=res.headers;
            return res.json() })
        .then(json => {
            return {
            data: json,
            total: parseInt(headers.get('content-range').split('/').pop(), 10),
            }
        });
    },
};

export default myDataProvider;